#!/usr/bin/env bash

# Just in case... This script is ran by the postinstall event of `npm install`
if ! [[ -d node_modules ]]
then
	echo "node_modules not found!"
	exit
fi

# Create an empty lib directory
rm -rf lib/
mkdir -p lib/

# Copy all the files we need from various libraries
cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js lib/
cp node_modules/jquery/dist/jquery.min.js lib/
cp node_modules/@popperjs/core/dist/umd/popper.min.js lib/
cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff2 lib/
