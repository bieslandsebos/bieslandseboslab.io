---
layout: default
title: Reserveren
lang: en
permalink: /en/booking/tnx.html
---

# Thanks for contacting us

Thank you for contacting us.

Only after our answer, any agreement is binding.

Normally, we reply within a few days. In case you haven't received any response within a week, please send an email to our booking office: [{{ site.email }}](mailto:{{ site.email }})
