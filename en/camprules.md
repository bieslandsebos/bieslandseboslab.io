---
layout: default
title: Camp rules
lang: en
---

# Camp rules

{% for sectie in site.data.regels.kampregels %}

## {{ sectie.kop.en }}

{% if sectie.intro %}{{ sectie.intro.en }}{% endif %}

{% for regel in sectie.regels %}
- {{ regel.en }}
{% endfor %}

{% endfor %}
