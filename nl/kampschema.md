---
layout: default
title: Kampschema
permalink: /Kampschema/index.html
sitemap: false
---

# Kampschema

Hieronder zie je het kampschema voor het komende seizoen.
De namen zijn ingevuld op basis van ieders voorkeuren en met ruimte voor de nieuwe aanwas in het kampstafbestand.

[Klik hier om het schema te bekijken en/of aan te passen](https://docs.google.com/spreadsheets/d/1IdzHVNP9HMyVCEHLdRPfROzbh11fhcMyZ9IvUt3J53E/edit).

Dubbelklik op een cel om in die cel te wijzigen en je naam in te vullen.
Ook bijvoorbeeld bij onderling ruilen of als een reservenaam wilt invullen.
Als je direct klikt en tikt overschrijf je de inhoud.
Pas dus op bij wijzigen van dingen.
Als je per ongeluk wat weggooit wat je niet wilt, kies dan in het menu voor `Bewerken`, `Ongedaan maken`.

De gekleurde cellen geven aan dat het dan vakantie is (grijs) of om een mid-week gaat (lichtblauw).

Alvast bedankt,  
De Beheerscommissie
