---
layout: default
title: Kampregels
---

# Kampregels

{% for sectie in site.data.regels.kampregels %}

## {{ sectie.kop.nl }}

{% if sectie.intro %}{{ sectie.intro.nl }}{% endif %}

{% for regel in sectie.regels %}
- {{ regel.nl }}
{% endfor %}

{% endfor %}
