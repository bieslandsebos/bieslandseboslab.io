# My own simple "format_money" filter
# jekyll-money is outdated
# There is no native liquid filter to format a number

module Jekyll
  module NumberFilter
    def format_money(amount)

      # 2 decimals
      fmt = "%0.2f" % amount

      # comma as decimal separator
      fmt["."] = ","

      # put euro sign in front of the amount
      "&euro; " + fmt

    end
  end
end

Liquid::Template.register_filter(Jekyll::NumberFilter)
