---
layout: post
title: In memoriam Hetty Hartkamp
date: 2023-06-07 22:29:21 +02:00
lang: nl
frontpage:
  until: 2023-06-30 23:59:59 +02:00
  excerpt: Op 4 juni 2023 overleed Hetty Hartkamp. Zij was meer dan 30 jaar het eerste aanspreekpunt voor onze kampeerders die een plek wilden reserveren en een drijvende kracht achter ons Scoutingkampeerterrein.
---

<div class="col-md-4 float-end">
	<img class="img-rounded img-fluid float-end" src="{% link assets/images/posts/hetty-hartkamp.jpg %}" alt="Hetty Hartkamp" title="Hetty Hartkamp" />
</div>

Op 4 juni 2023 overleed Hetty Hartkamp. Zij was meer dan 30 jaar het eerste aanspreekpunt voor onze kampeerders, die een plek wilden reserveren en een drijvende kracht achter ons Scoutingkampeerterrein.

Vanaf de oprichting van het Scoutingkampeerterrein Bieslandse Bos was Hetty al betrokken. Ze heeft eerst via de telefoon en later via de e-mail en het reserveringssysteem de contacten met de scoutinggroepen en andere kampeerders onderhouden. Haar grote betrokkenheid en gedrevenheid was tot het einde van haar leven voor iedereen zichtbaar.

Nadat haar man en, ook onze, steun en toeverlaat, Jan Hartkamp was overleden heeft zij zich weer weten op te richten. Met Hetty verliezen wij een vrijwilliger die ons scherp hield en onbaatzuchtig veel tijd heeft gestoken in het Bieslandse Bos. We zijn dankbaar voor al haar inzet en zullen haar blijven herinneren als echte Girl-scout.

We wensen haar familie en vrienden veel sterkte bij dit overlijden.

<div class="col-md-12 text-center">
	<img class="img-rounded img-fluid float-center" src="{% link assets/images/posts/einde-spoor.png %}" alt="Einde spoor" title="Einde spoor" />
</div>

------

Correspondentieadres: Noordweg 101, 2641 ZA Pijnacker
