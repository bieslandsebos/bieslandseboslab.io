---
layout: post
title: Bronzen kunstwerk gestolen
date: 2023-06-14 12:28:08 +02:00
lang: nl
frontpage:
  until: 2023-07-14 23:59:59 +02:00
  excerpt: Op ons terrein is zondag 11 juni een bronzen plaquette van hele hoge emotionele waarde gestolen.
---

<div class="col-md-4 float-end">
    <img class="img-rounded img-fluid float-end" src="{% link assets/images/posts/gestolen-bronzen-plaquette.jpg %}" alt="Gestolen bronzen plaquette" title="Gestolen bronzen plaquette" />
</div>

Op ons terrein is zondag 11 juni 2023 een bronzen plaquette en een fiets gestolen.
Met name de plaquette willen we graag terug, want het heeft voor ons een hele hoge emotionele waarde.

## Plaquette

Het is een klein kunstwerkje: een reliëf van dertig bij dertig centimeter.
Er zijn twee zwaluwen afgebeeld.
Het beeldje is dertig jaar geleden door een kunstenaar gemaakt voor de opening van het terrein.

## Overleden oprichters

De plaquette heeft een hele hoge emotionele waarde.
Het is dertig jaar geleden gemaakt door de oprichters van het Scoutingkampeerterrein, die nu alle drie overleden zijn.
Van de laatste hebben we [gisteren afscheid genomen]({% post_url 2023-06-07-in-memoriam-hetty-hartkamp %}) en juist bij die bijeenkomst is de diefstal ontdekt.
De emotionele waarde van dat bronzen beeld is duizend keer hoger dan de metaalwaarde van nog geen tien euro.

## Oproep

Daarom doen we nu de oproep om het beeldje terug te geven.
Wij hopen van harte dat het lukt om te zorgen dat het bij ons terug komt.

Weet iemand wie het beeldje heeft meegenomen of ben jij wellicht degene die het beeldje nu heeft?
Breng deze dan bij ons terug aan de Noordweg 101 in Pijnacker.
