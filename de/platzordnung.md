---
layout: default
title: Platzordnung
lang: de
---

# Platzordnung


{% for sectie in site.data.regels.kampregels %}

## {{ sectie.kop.de }}

{% if sectie.intro %}{{ sectie.intro.de }}{% endif %}

{% for regel in sectie.regels %}
- {{ regel.de }}
{% endfor %}

{% endfor %}
