/**
 * Update all the information we have about a field or facility
 */

function show_field_info(event)
{
	// Hide all the info divs (including the current one)
	$(".veld-info").hide();

	// Remove the active class from all the rect in the SVG (including the current one)
	$('[id^="veld_"]').removeClass("active");
	$('[id^="fac_"]').removeClass("active");

	// Set the clicked item as active
	$(this).addClass("active");

	// Construct the ID of the corresponding div that contains the information
	var info_id = "#list_" + event.target.id;

	// And show it
	$(info_id).show();

	// Now we need to update the image of the field/facility
	info_id += ">div>img";

	// If not yet set, replace "loading.svg" with the real image
	if ($(info_id).attr("data-img"))
	{
		$(info_id).attr("src", $(info_id).attr("data-img"));
		$(info_id).removeAttr("data-img");
	}
}

$(document).ready(function() {
	$('[id^="veld_"]').click(show_field_info);
	$('[id^="fac_"]').click(show_field_info);
});
