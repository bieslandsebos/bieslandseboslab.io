livereload:
	bundle exec jekyll serve --config _config.yml,_config-dev.yml --livereload

serve:
	bundle exec jekyll serve --config _config.yml,_config-dev.yml

build:
	JEKYLL_ENV=production bundle exec jekyll build --config _config.yml,_config-production.yml

rebuild: veryclean prepare build

prepare:
	if [ -f /.dockerenv ]; then npm install --unsafe-perm; else npm install; fi
	bundle config --local path _vendor
	bundle install
	[ -f _config-dev.yml ] || touch _config-dev.yml

clean:
	rm -rf .jekyll-cache/ .sass-cache/ _site/

veryclean: clean
	rm -rf Gemfile.lock package-lock.json .bundle/ lib/ node_modules/ _vendor/
